package bankAccount;

import java.awt.Color;
import java.awt.Font;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class View {
	
	JFrame frame;
	JPanel panel;
	JButton addPers, removePers, addAcc, removeAcc, addMoney, extractMoney;
	JLabel name, cnp, age, Accnr, namer, cnpr, ager, Accnrr, addSum, extrSum;
	JTextField nameTxt, cnpTxt, ageTxt, AccnrTxt, nameTxtr, cnpTxtr, ageTxtr, AccnrTxtr, addSumTxt, extrSumTxt;
	JTable table;
	JScrollPane scroll;
	
	public View(int unu) {
		new Controller();
	}
	
	public View() {
		// window
		frame = new JFrame("Banking account management");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 700);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		// panel
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.LIGHT_GRAY);
		
		// buttons
		addPers = new JButton("add person");
		addPers.setBounds(30, 30, 150, 40);
		removePers = new JButton("remove person");
		removePers.setBounds(30, 80, 150, 40);
		addAcc = new JButton("add account");
		addAcc.setBounds(30, 130, 150, 40);
		removeAcc = new JButton("remove account");
		removeAcc.setBounds(30, 180, 150, 40);
		addMoney = new JButton("add money");
		addMoney.setBounds(30, 230, 150, 40);
		extractMoney = new JButton("extract money");
		extractMoney.setBounds(30, 280, 150, 40);
		
		// lables
		name = new JLabel("name");
		name.setBounds(190, 35, 50, 30);
		cnp = new JLabel("cnp");
		cnp.setBounds(390, 35, 50, 30);
		age = new JLabel("age");
		age.setBounds(590, 35, 50, 30);
		namer = new JLabel("name");
		namer.setBounds(190, 85, 50, 30);
		cnpr = new JLabel("cnp");
		cnpr.setBounds(390, 85, 50, 30);
		ager = new JLabel("age");
		ager.setBounds(590, 85, 50, 30);
		Accnr = new JLabel("acc");
		Accnr.setBounds(190, 135, 50, 30);
		Accnrr = new JLabel("acc");
		Accnrr.setBounds(190, 185, 50, 30);
		addSum = new JLabel("sum");
		addSum.setBounds(190, 235, 50, 30);
		extrSum = new JLabel("sum");
		extrSum.setBounds(190, 285, 50, 30);
		
		// text fields
		nameTxt = new JTextField();
		nameTxt.setBounds(230, 35, 150, 30);
		cnpTxt = new JTextField();
		cnpTxt.setBounds(430, 35, 150, 30);
		ageTxt =  new JTextField();
		ageTxt.setBounds(630, 35, 50, 30);
		AccnrTxt = new JTextField();
		AccnrTxt.setBounds(230, 135, 100, 30);
		nameTxtr = new JTextField();
		nameTxtr.setBounds(230, 85, 150, 30);
		cnpTxtr = new JTextField();
		cnpTxtr.setBounds(430, 85, 150, 30);
		ageTxtr =  new JTextField();
		ageTxtr.setBounds(630, 85, 50, 30);
		AccnrTxtr = new JTextField();
		AccnrTxtr.setBounds(230, 185, 100, 30);
		addSumTxt = new JTextField();
		addSumTxt.setBounds(230, 235, 100, 30);
		extrSumTxt = new JTextField();
		extrSumTxt.setBounds(230, 285, 100, 30);
		
		// table
		Object[] columns = {"name", "cnp", "age", "account"};
		DefaultTableModel model = new DefaultTableModel(null, columns);
		table = new JTable(model);
		
		/*Person person = new Person();
		ArrayList<Person> personList = new ArrayList<Person>();
		personList = person.showPerson();
		
		for(Person p : personList){
			model.addRow(new Object[] {p.getName(),p.getCnp(), p.getAge()});
		}	*/
		
		Font font = new Font("", 1, 15);
		table.setFont(font);
		table.setRowHeight(30);
		
		// scroll
		scroll = new JScrollPane(table);
		scroll.setBounds(30, 350, 700, 300);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setVisible(true);
		
		// add in panel
		// buttons
		panel.add(addPers);
		panel.add(removePers);
		panel.add(addAcc);
		panel.add(removeAcc);
		panel.add(addMoney);
		panel.add(extractMoney);
		// labels
		panel.add(name);
		panel.add(cnp);
		panel.add(age);
		panel.add(Accnr);
		panel.add(addSum);
		panel.add(extrSum);
		// labels for remove
		panel.add(namer);
		panel.add(cnpr);
		panel.add(ager);
		panel.add(Accnrr);
		// text fields
		panel.add(nameTxt);
		panel.add(cnpTxt);
		panel.add(ageTxt);
		panel.add(AccnrTxt);
		panel.add(addSumTxt);
		panel.add(extrSumTxt);
		// text fields for remove
		panel.add(nameTxtr);
		panel.add(cnpTxtr);
		panel.add(ageTxtr);
		panel.add(AccnrTxtr);
		// table
		panel.add(scroll);
		
		// add in window
		frame.add(panel);
		
		frame.setVisible(true);
		
	}
		
}
