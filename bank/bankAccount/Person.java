package bankAccount;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;
 
public class Person implements Observer, Serializable {
	
	private static final long serialVersionUID = 1L;
	private int cnp, age;
	private String name;
	
	private Bank bank;
	
	public Person() {
	}
	
	public Person(String name, int cnp, int age) {
		this.name = name;
		this.cnp = cnp;
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
		
	public int getCnp() {
		return cnp;
	}

	public void setCnp(int cnp) {
		this.cnp = cnp;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(getCnp());
		out.writeObject(getName());
		out.writeInt(getAge());
	}

	public void readExternal(ObjectInput in) throws IOException {
		setCnp(in.readInt());
		try {
			setName((String) in.readObject());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		setAge(in.readInt());
	}
	
	private static ArrayList<Person> arrToAL(Person[] val)
	{
		ArrayList<Person> toreturn = new ArrayList<Person>(val.length);
		for(Person p : val)
		{
			toreturn.add(p);
		}
		return toreturn;
	}
	
	public ArrayList<Person> showPerson() {
		
		return arrToAL((Person[]) bank.hash.values().toArray());
		
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		return prime * result + this.getCnp();
	}
	
	public static void updateAdd() {
		JOptionPane.showMessageDialog(null, "info", "1 new person was added!", JOptionPane.INFORMATION_MESSAGE);	
	}
	
	public static void updateRemove() {
		JOptionPane.showMessageDialog(null, "info", "1 new person was removed!", JOptionPane.INFORMATION_MESSAGE);	
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
}
