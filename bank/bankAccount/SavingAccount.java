package bankAccount;

public class SavingAccount extends Account {

	private static final long serialVersionUID = 1L;

	public SavingAccount(int nrAcc) {
		super(nrAcc);
	}
	
	public void depunere(int newSum) {
		this.setSum(this.getSum()+newSum);
		
	}
}
