package bankAccount;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Observable;
import java.util.Random;
import javax.swing.JOptionPane;


public class Account extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;
	public int nrAcc, sum;
	
	public Account(int nrAcc) {
		this.nrAcc = nrAcc;
		this.sum = new Random().nextInt(1000);
	}

	public void setSum(int sum) {
		this.sum = sum;
	}
	
	public int getSum() {
		return sum;
	}
	
	public void setNrAcc(int nr) {
		this.nrAcc = nr;
	}

	public int getNrAcc() {
		return nrAcc;
	}
	
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(getNrAcc());
		out.writeInt(getSum());
	}

	public void readExternal(ObjectInput in) throws IOException {
		setNrAcc(in.readInt());
		setSum(in.readInt());
	}
	
	public static void updateAdd() {
		JOptionPane.showMessageDialog(null, "info", "1 new account was added!", JOptionPane.INFORMATION_MESSAGE);	
	}
	
	public static void updateRemove() {
		JOptionPane.showMessageDialog(null, "info", "1 new account was removed!", JOptionPane.INFORMATION_MESSAGE);	
	}
	
}
