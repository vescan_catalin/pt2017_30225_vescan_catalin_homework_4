package bankAccount;

public class SpandingAccount extends Account{

	private static final long serialVersionUID = 1L;
	private float comision = (float) 0.2;
	
	public SpandingAccount(int nrAcc) {
		super(nrAcc);
	}
	
	public float getComision() {
		return comision;
	}
	
	public void retragere(int newSum) {
		this.setSum(this.getSum() - newSum - (int)(newSum * comision));
	}

}
