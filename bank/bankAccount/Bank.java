package bankAccount;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = 1L;
	public HashMap<Person, HashSet<Account>> hash;
	public transient File file;
	
	public Bank() {
		hash = new HashMap<Person, HashSet<Account>>();
		file = new File("Account.txt");
	}
	
	public Bank(File file) {
		this.hash = new HashMap<Person, HashSet<Account>>();
		this.file = file;
	}
	
	@Override
	public void addPerson(Person person) {
		hash.put(person, new HashSet<Account>());
		Person.updateAdd();
	}

	@Override
	public void removePerson(Person person) {
		hash.remove(person);
		Person.updateRemove();
	}

	@Override
	public void addAccount(Person person, Account account) {
		hash.get(person).add(account);
		Account.updateAdd();
	}

	@Override
	public void removeAccount(Person person, Account account) {
		hash.get(person).remove(account);	
		Account.updateRemove();
	}

	@Override
	public void writeData() {
		ObjectOutputStream objOutputStream = null;
		try {
			objOutputStream = new ObjectOutputStream(new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		try {
			objOutputStream.writeObject(hash);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void readData() {
		ObjectInputStream objInputStream = null;
		try {
			objInputStream = new ObjectInputStream(new FileInputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			hash = (HashMap<Person, HashSet<Account>>) objInputStream.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
