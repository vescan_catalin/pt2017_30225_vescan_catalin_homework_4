package bankAccount;

public interface BankProc {
	
	public void addPerson(Person person);
	public void removePerson(Person person);
	public void addAccount(Person person, Account account);
	public void removeAccount(Person person, Account account);
	public void writeData();
	public void readData();
	
}
