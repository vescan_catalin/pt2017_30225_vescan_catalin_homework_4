package bankAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class Controller {

	private View view = new View();
	private Bank bank = new Bank();

	public Controller() {
		view.addPers.addActionListener(new ActionEvents());
		view.removePers.addActionListener(new ActionEvents());
		view.addAcc.addActionListener(new ActionEvents());
		view.removeAcc.addActionListener(new ActionEvents());
		view.addMoney.addActionListener(new ActionEvents());
		view.extractMoney.addActionListener(new ActionEvents());
	}
	
	public class ActionEvents implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == view.addPers) {
				bank.addPerson(new Person(view.nameTxt.getText(), Integer.parseInt(view.cnpTxt.getText()), Integer.parseInt(view.ageTxt.getText())));
				bank.writeData();
			}
			if(e.getSource() == view.removePers) {
				bank.readData();
				Person p = new Person(view.nameTxtr.getText(), Integer.parseInt(view.cnpTxtr.getText()), Integer.parseInt(view.ageTxtr.getText()));
				if(bank.hash.containsKey(p)) {
					bank.removePerson(p);
					bank.writeData();
				}
				else
					JOptionPane.showMessageDialog(null, "", "this person doesn't exist", JOptionPane.INFORMATION_MESSAGE);
			}
			if(e.getSource() == view.addAcc) {
				Person p = new Person(view.nameTxt.getText(), Integer.parseInt(view.cnpTxt.getText()), Integer.parseInt(view.ageTxt.getText()));
				Account a = new Account(Integer.parseInt(view.AccnrTxt.getText()));
				bank.addPerson(p);
				bank.addAccount(p, a);
				bank.writeData();
			}
			if(e.getSource() == view.removeAcc) {
				bank.readData();
				Person p = new Person(view.nameTxtr.getText(), Integer.parseInt(view.cnpTxtr.getText()), Integer.parseInt(view.ageTxtr.getText()));
				Account a = new Account(Integer.parseInt(view.AccnrTxtr.getText()));
				if(bank.hash.keySet().stream().filter(x->x.getName().compareTo(p.getName())==0).count()==1) {
					System.out.println("persoana exista");
					if(bank.hash.containsValue(a)) {
						System.out.println("contul exista");
						bank.removeAccount(p, a);
						bank.writeData();
					}
					else
						JOptionPane.showMessageDialog(null, "", "this account doesn't exist", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "", "this person doesn't exist", JOptionPane.INFORMATION_MESSAGE);
			}	
			if(e.getSource() == view.addMoney) {
				SavingAccount a = new SavingAccount(Integer.parseInt(view.AccnrTxt.getText()));
				a.depunere(Integer.parseInt(view.addSumTxt.getText()));
			}				
			if(e.getSource() == view.extractMoney) {
				SpandingAccount a = new SpandingAccount(Integer.parseInt(view.AccnrTxt.getText()));
				System.out.println(a.sum);
				a.retragere(Integer.parseInt(view.extrSumTxt.getText()));
				System.out.println(a.sum);
			}
			
		}
		
	}
	
	
	
}
